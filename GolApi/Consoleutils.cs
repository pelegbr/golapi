﻿using System;
using System.Runtime.InteropServices;

namespace GolApi {
	/// <summary>
	/// This is a utility class for enabling a console in out of IDE debug build.
	/// </summary>
	public static class ConsoleUtils {
		[DllImport("kernel32.dll", EntryPoint = "AllocConsole", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]	
		private static extern int AllocConsole();
		
		/// <summary>
		/// This funciton allocates a console window for debug builds outside the IDE.
		/// </summary>
		/// <returns>
		/// true, if succeeded false otherwise.
		/// </returns>
		public static bool AllocateConsole() {
			return AllocConsole() != 0;
		}
		
	}
	
}
