﻿using System;
using System.Collections.Generic;

namespace GolApi.Event {
	/// <summary>
	/// This class is the parent of all events in GolApi.
	/// </summary>
	public abstract class Eventbase {
		//The arguments for the event.
		protected List<int> args;
		
		/// <summary>
		/// This constructor takes a number of integers and creates an event.
		/// </summary>
		protected Eventbase(params int[] args) {
			this.args = new List<int>();
			this.args.AddRange(args);
		}
		
		/// <summary>
		/// Supplies teh arguments given to an event.
		/// </summary>
		public abstract List<int> GetArgs(int[] currentArgs);
		/// <returns> The Unique Id of this type of event. </returns>
		public abstract int GetId();
		/// <summary>
		/// This function Dispatches an event to the right handler.
		/// </summary>
		/// <returns> True if the event was handled, false otherwise. </returns>
		public abstract bool Dispatch();

	}
	
	/// <summary>
	/// This class is the basis of a task for tile in the game.
	/// </summary>
	public abstract class Task {
		//The event of the task.
		protected Eventbase Base;
		
		/// <summary>
		/// This constructor creates a task from an event.
		/// </summary>
		protected Task(Eventbase e) {
			Base = e;
		}
		
		/// <returns> Ehe event of this task. </returns>
		public Eventbase GetEvent() {
			return Base;
		}

	}
	
	/// <summary>
	/// This enum dictates what type of a task a task is, Movement, Death or Revival.
	public enum TaskType {
		MOVE,
		DIE,
		REVIVE
	}
}
