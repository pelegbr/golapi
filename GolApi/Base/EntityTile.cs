﻿using System;
using System.Drawing;
using gol;
using GolApi.Event;
using GolApi.impl.Schedule;

namespace GolApi.Base {
	/// <summary>
	/// This class represents a dynamic tile.
	/// This tile can move, draw itselt and update itself
	/// </summary>
	public abstract class EntityTile : Tile {
		///A task schedule.
		protected Schedule<Task> Schedule;
		//A movement schedule.
		protected MovementSchedule MoveSchedule;
		//Is any of the schedules not empty.
		protected bool IsScheduled;

		/// <summary>
		/// This constructor creates an entity from it's co-ordinates.
		/// </summary>
		/// <param name="x"> The x co-ordinates of the entity. </param>
		/// <param name="y"> The y co-ordinates of the entity. </param>
		protected EntityTile(int x, int y) : base(x, y) { }

		/// <summary>
		/// Calculates the current dynamic position of the entity.
		/// </summary>
		/// </returns> the current dynamic position of the entity. </returns>
		public abstract TilePosition GetUpdatedPosition();

		/// <summary> 
		/// This function draws an entity to a control.
		/// </summary>
		/// <param name="graphics"> The graphics object of the control. </param>
		/// <param name="scale"> The scale at which to draw the entity. </param>
		/// <param name="xOffset"> The x co-ordinate of start of the usable area in the control. </param>
		/// <param name="yOffset"> The y co-ordinate of start of the usable area in the control. </param>		
		public abstract void Draw(Graphics graphics, int scale, int xOffset, int yOffset);
		
		/// <summary>
		/// This function draws the player tile and the one replaced by it as a result of a movement instead of the whole board.
		/// Used to draw player movement.
		/// </summary>
		/// <param name="graphics"> The graphics object of the control. </param>
		/// <param name="size"> The size of the control. </param>
		/// <param name="background"> The background image of the control. </param>
		/// <param name="xOffset"> The x co-ordinate of start of the usable area in the control. </param>
		/// <param name="yOffset"> The y co-ordinate of start of the usable area in the control. </param>
		public abstract void DrawMovement(Graphics graphics, Size size, Bitmap background, int scale, int xOffset, int yOffset);

		/// <summary>
		/// This function updates a tile when a turn is generating.
		/// </summary>
		/// <param name="b"> The board object. </param>
		public abstract void Update(Board b);
		
		/// <summary>
		/// This function schedules a task to the entity's schedule or movement schedule.
		/// </summary>
		/// <param name="t"> The task to schedule. </param>
		public abstract void ScheduleTask(Task t);
	}
}
