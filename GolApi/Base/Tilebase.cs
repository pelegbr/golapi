﻿
using System;
using gol;
using GolApi.Event;
using GolApi.impl.Schedule;

namespace GolApi.Base {
	/// <summary>
	/// This class represents a tile at the board.
	/// </summary>
	public abstract class Tile {
		//The position of this tile.
		protected TilePosition Position;
		//Is the tile dead.
		protected bool Dead = false;
		
		/// <summary>
		/// This constructor takes co-ordinates for a tile.
		/// </summary>
		/// <param name="x"> The x co-ordinate for the tile. </param>
		/// <param name="y"> The y co-ordinate for the tile. </param>
		protected Tile(int x, int y) {
			Position = new TilePosition(x, y);
		}
		
		/// <returns> True if this tile is scheduled to die, false otherwise </returns>
		public abstract bool IsDead();
		
		///<summary>
		/// This function schedules a tile's death.
		/// </summary>
		/// <param name="b"> The board object. </param>
		public void Schedulekill(Board b) {
			Dead = true;
			b.Statistics.NotifyDeadTile(this);
		}
		
		/// <returns> this tile's position. </returns>
		public TilePosition GetPosition() {
			return Position;
		}
		
		/// <summary>
		/// Cancels death for this tile.
		/// </summary>
		public void CencalDeath(Board b) {
			Dead = false;
			b.Statistics.NotifyDeathCencal(this);
		}
	}
}
