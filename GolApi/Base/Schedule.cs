﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GolApi.Event;

namespace GolApi.Base {
	
	/// <summary>
	/// This class holds tasks for entities.
	/// </summary>
	public abstract class Schedule<Taskbase>where Taskbase : Task {
		//The tasks awaiting accomplishment.
		protected Collection<Taskbase> Tasks;
		//The task type avialable for this Schedule.
		public TaskType TaskType;
		// The data of the schedule.
		protected List<int> Data;
		
		/// <returns> True if any task is scheduled, false otherwise. </returns>
		public bool HasSchedule() {
			return Tasks.Count != 0;
		}
	
		/// <summary>
		/// Adds a task to the schedule.
		/// </summary>
		public void AddTask(Taskbase e) {
			Tasks.Add(e);
		}
		
		/// <summary>
		/// This constructor initializes an empty schedule.
		/// </summary>
		protected Schedule() {
			Tasks = new Collection<Taskbase>();
			Data = new List<int>();
		}
		
		/// <summary>
		/// Perfoms all tasks in the schedule.
		/// </summary>
		/// <returns> this. </summary>
		public Schedule<Taskbase> ApplySchedule() {
			int i = 0;
			bool handled = false;
			while (i < Tasks.Count && !handled) {
				
				if (Tasks[i].GetEvent().GetId() != (int)TaskType)
					break;
				
				handled = Tasks[i].GetEvent().Dispatch();
				Data = Tasks[i].GetEvent().GetArgs(Data.ToArray());
				Tasks.RemoveAt(i);
				
				i++;
			}
			return this;
		}

		/// <summary>
		/// Sets the data of this schedule.
		/// </summary>
		/// <param name="data"> The new data. </param>
		public virtual void SetData(params int[] data) {
			Data = new List<int>(data);
		}

		/// <summary>
		/// This function gets the data of this schedule and then resets it all to default.
		/// </summary>
		/// <returns> The previous data. </returns>
		public virtual int[] PopData() {
			int[] data = Data.ToArray();
			Data = Data.ConvertAll(i => 0);
			return data;
		}

		/// <returns> The current data. </returns>
		public int[] GetData() {
			return Data.ToArray();
		}
	}
}
