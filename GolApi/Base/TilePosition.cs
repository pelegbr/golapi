﻿using System;

namespace GolApi.Base {
	/// <summary>
	/// This class represents a tile's co-ordinates on a board.
	/// </summary>
	public class TilePosition {
		//The tile's co-ordinates.
		int X, Y;
		
		/// <returns> The x co-ordinate of the tile. </returns>
		public int GetX() {
			return X;
		}
		
		/// <returns> The x co-ordinate of the tile. </returns>
		public int GetY() {
			return Y;
		}

		/// <returns> The co-ordinates of the tile as a tuple. </returns>
		public (int X, int Y) ToTuple() {
			return (X, Y);
		}
		/// <summary>
		/// This is the default constructor for (0, 0).
		/// </summary>
		TilePosition() : this(0, 0) {
			
		}
		
		/// <returns> the co-ordinates of the tile as a [x, y] array of ints. </returns>
		public int[] ToArray() {
			return new int[]{ X, Y};
		}

		/// <summary>
		/// A simple constructor taking the x and y co-ordinates of the tile.
		/// </summary>
		/// <param name="x"> The x co-ordinate of the tile. </param>
		/// <param name="y"> The y co-ordinate of the tile. </param>
		public TilePosition(int x, int y) {
			X = x;
			Y = y;
		}
		
		/// <summary>
		/// This is a copy constructor.
		/// </summary>
		/// <param name="tp> the position to copy. </param>
		public TilePosition(TilePosition tp) {
			X = tp.X;
			Y = tp.Y;
		}
		
		/// <summary>
		/// Clones this position to a new object.
		/// </summary>
		public TilePosition Copy() {
			return new TilePosition(this);
		}

		/// <summary>
		/// Operator overload for equals.
		/// </summary>
		/// <param name="t"> The first position to equate. </param>
		/// <param name="other"> The second position to equate. </param>
		public static bool operator==(TilePosition t, TilePosition other) {
			return other.X == t.X && other.Y == t.Y;
		}

		/// <summary>
		/// Operator overload for not equals.
		/// </summary>
		/// <param name="t"> The first position to equate. </param>
		/// <param name="other"> The second position to equate. </param>
		public static bool operator!=(TilePosition t, TilePosition other) {
			return other.X != t.X || other.Y != t.Y;
		}

		/// <summary>
		/// Decides if an object is equal to this.
		/// </summary>
		/// <param name="obj"> The object to equate to this. </param>
		public override bool Equals(object obj) {
			return obj is TilePosition tile && tile.X == X && tile.Y == Y;
		}

		/// <summary>
		/// Generates hash code representing the co-ordinates of this position.
		/// </summary>
		public override int GetHashCode() {
			return X + Y >> 4;
		}

		/// <summary>
		/// Sets this co-ordinate to a new set of co-ordinate supplied.
		/// </summary>
		/// <param name="x"> The x co-ordinate of the new co-ordinates. </param>
		/// <param name="y"> The y co-ordinate of the new co-ordinates. </param>
		public TilePosition set(int x, int y) {
			X = x;
			Y = y;
			return this;
		}
	}
}
