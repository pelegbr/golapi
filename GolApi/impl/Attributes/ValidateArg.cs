﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace GolApi.impl.Attributes {

	public static class ValidateArg {
		/// <summary>
		/// This function validates parameters given to a function, checking for ParameterAttributes.
		/// </summary>
		public static void HandleParameters(StackFrame sf, params object[] values) {
			//Gets the function.
			var method = sf.GetMethod();
			//Gets the parameters given to it.
			var parameters = method.GetParameters();
			var currentOBJ = 0;
			
			//Checks for each paramter if it has a ParameterAttribute, if it as it invokes ParameterAttribute.Handle(parameter).
			for (int i = 0; i < parameters.Length; i++) {
				for (int j = 0; j < parameters[i].GetCustomAttributes(false).Length; j++) {
					if (parameters[i].GetCustomAttributes(false)[j] is ParameterAttribute) {
						((ParameterAttribute)parameters[i].GetCustomAttributes(false)[0]).Handle(values[currentOBJ++]);
					}
				}
			}
			
		}
	}
}
