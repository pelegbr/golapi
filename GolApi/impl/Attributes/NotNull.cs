﻿using System;
using System.Runtime.InteropServices;

namespace GolApi.impl.Attributes {
	/// <summary>
	/// This attribute represents a value which can not be null, it can only apply to parameters.
	/// </summary>
	public class NotNullAttribute : ParameterAttribute {
		/// <summary>
		/// This function handles a value, if it it null it throws new NullReferenceException.
		/// </summary>
		public override void Handle(object o) {
			if (o == null)
				throw new NullReferenceException("NonNull Parameter type was set to null");
		}
	}
	
	/// <summary>
	/// This is an abstract attribute that can check a value for validity, it can only apply to parameters.
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter)]
	[ComVisible(true)]
	public  abstract class ParameterAttribute : Attribute {
		/// <summary>
		/// This function checks if a value is valid, if not throw an exception.
		/// </summary>
		public abstract void Handle(object o);
	}

}
