﻿using System;
using GolApi.Base;
using GolApi.Event;

namespace GolApi.impl.Schedule {
	/// <summary>
	///This class is an empty implementation of a Schedule<Task>.
	/// </summary>
	public class DynamicSchedule : Schedule<Task> {
		
	}
}
