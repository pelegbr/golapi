﻿using System;
using System.Collections.Generic;
using GolApi.Base;
using GolApi.impl.Tasks;

namespace GolApi.impl.Schedule {
	/// <summary>
	/// This class is a Schedule for movement tasks.
	/// <summary>
	public class MovementSchedule : Schedule<TaskMove> {
		// Constant indexes of co-ordinates.
		public static readonly int X_POSITION = 0, Y_POSITION = 1;
		// The amount to move in the x axis.
		protected int X;
		// The amount to move in the y axis.
		protected int Y;
	
		/// <summary>
		// This constructor takes the initial state creates a schedule. 
		/// </summary>
		/// <param name="x"> The initial x co-ordinate for this schedule. </param>
		/// <param name="y"> The initial y co-ordinate for this schedule. </param>
		/// <param name="speed"> The initial speed for this schedule. </param>
		public MovementSchedule(int x, int y, int speed) : base() {	
			X = x;
			Y = y;
			Data.Add(x);
			Data.Add(y);
			Data.Add(speed);
		}
		
		/// <returns> The Applied x value. </returns>
		public int ApplyX() {
			X = Data[X_POSITION];
			return X;
		}
		
		/// <returns> The Applied y value. </returns>
		public int ApplyY() {
			Y = Data[Y_POSITION];
			return Y;
		}

		/// <summary>
		/// This function sets the data of this schedule
		/// </summary>
		/// <param name="data"> The new data for this schedule. </param>
		public override void SetData(params int[] data) {
			base.SetData(data);
			X = data[0];
			Y = data[1];
		}

		/// <summary> 
		/// This function gets the data of this schedule and then resets it.
		/// </summary>
		/// <returns> The previous data of this schedule. </returns>
		public override int[] PopData() {
			X = 0;
			Y = 0;
			return base.PopData();
		}
	}
}
