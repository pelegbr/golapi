﻿using System;
using gol;
using GolApi.Base;

namespace GolApi.impl.Tiles {
	/// <summary>
	/// This class is the default implementation of a static(not-entity) tile.
	public class CityTile : Tile {
		
		/// <summary>
		/// This constructor creates a city tile with co-ordinates.
		/// </summary>
		/// <param name="x"> The x co-ordinate of the tile's position. </param>
		/// <param name="y"> The y co-ordinate of the tile's position. </param>
		public CityTile(int x, int y) : base(x, y) {
		}

		/// <returns> true if the tile is scheduled to die, false otherwise. </returns>
		public override bool IsDead() {
			return Dead;
		}
		
	}
}
