﻿using System;
using System.Drawing;
using System.Threading;
using gol;
using GolApi.Base;
using GolApi.Event;
using GolApi.impl.Schedule;
using GolApi.impl.Tasks;

namespace GolApi.impl.Tiles.EntityTiles {
	/// <summary>
	/// The default implementation of a cleint interactable tile.
	/// </summary>
	public class PlayerTile : EntityTile {
		//The name of the player.
		readonly String Name;
		//The dynamic position of the player.
		volatile TilePosition DisplayPosition;
		//The last position the player was displayed at.
		TilePosition Last;
	
		/// <summary>
		/// This constructor creates a playable tile with a name and co-ordinate.
		/// </summary>
		/// <param name="name"> The name of the player. </param>
		/// <param name="x"> The x co-ordinate of the player </param>
		/// <param name="y"> The y co-ordinate of the player </param>
		public PlayerTile(String name, int x, int y) : base(x, y) {
			Schedule = new DynamicSchedule();
			MoveSchedule = new MovementSchedule(0, 0, 1);
			Name = name;
			DisplayPosition = Position;
			Last = Position;
		}
	
		/// <returns> the name of this player. </returns>
		public String GetName() {
			return Name;
		}
		
		/// <returns> true if the player is dead, else false.
		public override bool IsDead() {
			return Dead;
		}
		
		/// <summary>
		/// This function calculates the current position of the player and returns it.
		/// </summary>
		/// <returns> The calculated position of the player </returns>
		public override TilePosition GetUpdatedPosition() {
			MoveSchedule.ApplySchedule();
			int[] Data = MoveSchedule.PopData();
			return new TilePosition(DisplayPosition.GetX() + Data[0], DisplayPosition.GetY() + Data[1]);
		}
		
		/// <summary>
		/// This function is the update loop of the player, all player logic goes here namely movement logic.
		/// </summary>
		public override void Update(Board b) {
			int gen = b.GetGen();
			if(!b.GotoNext) {
				//Logic loop.
				new Thread(() => {
					//Lock input.
					b.Statistics.Lock.WaitOne();
					//While the turn hasn't ended.
					while(!b.GotoNext && b.GetGen() == gen) {
						//If the player has any task scheduled.
						if(IsScheduled) {
							//validate the new position and apply if valid.
							TilePosition candidate = GetUpdatedPosition();
							if(candidate.Equals(Position))
								DisplayPosition = Position;
							else if(b.IsInBounds(candidate) && b.IsTileEmpty(candidate))
								DisplayPosition = candidate;
							//All tasks are done.
							IsScheduled = false;
						}
						Thread.Sleep(1);
					}
					//At the end of the turn set non-dynamic position and release input lock.
					b.SetEntityPos(this, DisplayPosition);
					Last = Position = DisplayPosition;
					b.Statistics.Lock.ReleaseMutex();
				}).Start();
			}
		}

		/// <summary>
		/// This function draws a player on a control.
		/// </summary>
		/// <param name="graphics"> The graphics to draw on the control. </param>
		/// <param name="scale"> The scale at which to draw the player. </param>
		/// <param name="xOffset"> The start x co-ordinate usable in the control. </param>
		/// <param name="yOffset"> The start y co-ordinate usable in the control. </param>
		public override void Draw(Graphics graphics, int scale, int xOffset, int yOffset) {
			graphics.FillRectangle(Brushes.Yellow, new Rectangle(xOffset + DisplayPosition.GetX() * scale,yOffset + DisplayPosition.GetY() * scale, scale, scale));
		}

		/// <summary>
		/// This function draws a player after it changed position.
		/// </summary>
		/// <param name="graphics"> The graphics object of the control. </param>
		/// <param name="size"> The size of the control. </param>
		/// <param name="background"> The background image of the control. </param>
		/// <param name="xOffset"> The x co-ordinate of start of the usable area in the control. </param>
		/// <param name="yOffset"> The y co-ordinate of start of the usable area in the control. </param>
		public override void DrawMovement(Graphics graphics, Size size, Bitmap background, int scale, int xOffset, int yOffset) {
			//While has moving tasks.
			while(IsScheduled) ;
			//If the player didn't move don't draw.
			if(Last == DisplayPosition) return;
			//Calculate control -> background pixel ratio.
			double widthRat = background.Width / (double) size.Width;
			double heightRat = background.Height / (double) size.Height;
			//The Area to copy from the image.
			Rectangle rectangle = new Rectangle((int)((xOffset + Last.GetX() * scale + 1) * widthRat), (int)((yOffset + Last.GetY() * scale + 1) * heightRat), scale - 1, scale - 1);
			//Copied background area.
			Bitmap cropped = background.Clone(rectangle, background.PixelFormat);
			//Draw the empty tile
			graphics.DrawImage(cropped, new Point(xOffset + Last.GetX() * scale + 1, yOffset + Last.GetY() * scale + 1));
			graphics.DrawRectangle(Pens.Black, new Rectangle(xOffset + Last.GetX() * scale, yOffset + Last.GetY() * scale, scale, scale));
			//Draw the player.
			graphics.FillRectangle(Brushes.Yellow, new Rectangle(xOffset + DisplayPosition.GetX() * scale, yOffset + DisplayPosition.GetY() * scale, scale, scale));
			//Set last to this position.
			Last = DisplayPosition;
		}

		/// <summary>
		/// This function schedules a task for this player.
		/// </summary>
		/// <param name="t"> The task to schedule. </param>
		public override void ScheduleTask(Task t) {
			//Rout the task to either the movement Schedule or the dynamic schedule.
			if(t is TaskMove taskMove)
				MoveSchedule.AddTask(taskMove);
			else
				Schedule.AddTask(t);
			//The Schedule is now not empty.
			IsScheduled = true;
		}

	}
}
