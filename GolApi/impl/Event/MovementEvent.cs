﻿using System;
using System.Collections.Generic;
using GolApi.Base;

namespace GolApi.Event {
	/// <summary>
	/// This class represents a entity movement as an event.
	/// </summary>
	public class MovementEvent : Eventbase {
		//The Co-ordinates to move in the x and y axis.
		TilePosition MovePosition;
		//The speed to move at.
		int Speed;
		
		/// </summary>
		/// This constructor takes integers and creates a movement event from them.
		/// </summary>
		/// <param name="args"> The arguments for this event. </param>
		public MovementEvent(params int[] args) : base(args) {
			MovePosition = new TilePosition(args[0], args[1]);
			Speed = args[2];
		}

		/// <summary>
		/// This function adds the event's co-ordinates to given other co-ordinates.
		/// </summary>
		/// <param name="currentArgs"> The current position of an entity. </param>
		/// <returns> The new position of the entity </returns>
		public override List<int> GetArgs(int[] currentArgs) {
			return new List<int> {
				MovePosition.GetX() + currentArgs[0],
				MovePosition.GetY() + currentArgs[1],
				args[2]
			};
		}

		/// <returns> TaskType.Move, The id of a movement event. </returns>
		public override int GetId() {
			return (int)TaskType.MOVE;
		}

		/// <summary>
		/// This function is a dummy implementation.
		/// </summary>
		/// <returns> true, always. </returns>
		public override bool Dispatch() {
			return false;
		}

	}
}
