﻿/*
 * Created by SharpDevelop.
 * User: peleg br
 * Date: 6/5/2016
 * Time: 21:17
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using GolApi.Event;

namespace GolApi.impl.Tasks {
	/// <summary>
	/// This is a task that inacts movement of an entity.
	/// </summary>
	public class TaskMove : Task {
		/// <summary>
		/// This constructor Takes a movement event and creates a task to invoke it.
		/// </summary>
		/// <param name="e"> The event for this task. </param>
		public TaskMove(MovementEvent e) : base(e) {
			
		}
	}
}
