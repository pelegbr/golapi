﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using GolApi.Base;
using GolApi.impl.Attributes;
using GolApi.impl.Tiles;
using GolApi.impl.Tiles.EntityTiles;

namespace gol {
	/// <summary>
	/// This class is the main class of the game storing the board formation for the game, width, height, player and statistics.
	/// It also has a couple of utilities like draw, exportTiles etc'.
	/// </summary>
	public class Board{
		//The Board's formation for this game, all tiles.
		protected Tile[,] TileGrid;
		//The object holding the statistics about this game, like how many cells are alive, dead, entity and more.
		public Statistics Statistics { private set; get; } = new Statistics();
		//The Board's dimentions.
		protected int Height, Width;
		
		//Whether player input is enabled.
		public bool GotoNext;
		//The player tile.
		protected PlayerTile Player;
		//How much time it took to generate one turn.
		public long Generated = 0;
		
		/// <summary>
		/// This constructor takes the width and the height of the board and randomizes the formation of the game
		/// </summary>
		/// <param name="height"> The height of the board</param>
		/// <param name="width"> The width of the board</param>
		public Board(int height, int width) {
			//Generate a new array of tiles for the game's formation.
			TileGrid = new Tile[height, width];
			//Set local fields.
			Height = height;
			Width = width;
			//Randmizes the board.
			for(int x = 0; x < width; x++) {
				for(int y = 0; y < height; y++)
					if(Statistics.Rand.Next(0, 25) <= 10)
						TileGrid[x, y] = new CityTile(x, y);
			}
		}

		/// <summary>
		/// This function triggers the next turn of the game.
		/// </summary>
		public void Generate() {
			//Record how much time a turn takes.
			var ms = Stopwatch.GetTimestamp() / Stopwatch.Frequency * 1000;
			//Add a generation to the statistics object.
			Statistics.Genaration++;
			//Wait until the player's input loop is done.
			Statistics.Lock.WaitOne();
			//Instance shared tile position to avoid garbage collection.
			TilePosition tilePosition = new TilePosition(0, 0);
			//For each cell.
			for(var x = 0; x < Width; x++) {
				for (var y = 0; y < Height; y++) {
					//Get the current cell.
					var t = TileGrid[x, y];
					//Instance shared tile position.
					tilePosition.set(x, y);
					
					//#$Easteregg$#
					if(GetGen() == 42 * 10) {

						if(t == null)
							TileGrid[x, y] = new CityTile(x, y);
						else if(t.IsDead())
							TileGrid[x, y].CencalDeath(this);
					}
					//#$Easteregg_end$#
					//If a tile exists in (x, y).
					else if(t != null) {
						//Kill cells scheduled for death.
						if(t.IsDead())
							TileGrid[x, y] = null;
						else {
							//Either update the cell if entity or apply game rules if static
							if(t is EntityTile entityTile) {
								GotoNext &= !(entityTile is PlayerTile);
								entityTile.Update(this);
							} else {
								int neighbors = GetNeighbors(tilePosition);
								//Count the neighboring cells. if there are more than 3 or less than 2 kill this cell.
								if(neighbors > 3 || neighbors < 2)
									TileGrid[x, y].Schedulekill(this);
							}
						}
						//This function checks if a cell should be revived.
					} else if(IsRevived(tilePosition))
						CreateTile(x, y);
				}
			}
			Statistics.Lock.ReleaseMutex();
			Generated = Stopwatch.GetTimestamp() / Stopwatch.Frequency * 1000 - ms;
		}

		/// <summary>
		/// This function deterimes the maximum scale of a cell in the game's formation matrix.
		/// </summary>
		/// <param name="size"> The size of the containing control</param>
		/// <param name="xOffset"> The starting x co-ordinate of the control that is usable by the board</param>
		/// <param name="yOffset"> The starting y co-ordinate of the control that is usable by the board</param>
		/// <returns> the maximal scale for a single cell in the game's formation matrix. </returns>
		public int GetScale(Size size, int xOffset, int yOffset) {
			return Math.Min((size.Width - xOffset) / TileGrid.GetLength(0), (size.Height - yOffset) / TileGrid.GetLength(1));
		}

		/// <summary>
		/// This funciton creates a new static(City) Tile in the game's formation matrix.
		/// </summary>
		/// <param name="x"> The x co-ordinate to create the tile in</param>
		/// <param name="y"> The y co-ordinate to create the tile in</param>
		private void CreateTile(int x, int y) {
			TileGrid[x, y] = new CityTile(x, y);
			Statistics.SettlementsDead--;
		}

		/// <summary>
		/// This function checks if a TilePosition in inside the board's bounds.
		/// </summary>
		/// <param name="candidate"> The position to check inside bounds for </param>
		/// <returns> True if the position is inside the bounds of the board, false otherwise </param>
		public bool IsInBounds(TilePosition candidate) {
			return candidate.GetX() < TileGrid.GetLength(0) && 
				candidate.GetY() < TileGrid.GetLength(1) &&
				candidate.GetX() >= 0 &&
				candidate.GetY() >= 0;
		}

		/// <summary>
		/// This function sets Genaration to -1, signaling all running background threads to stop running.
		/// </summary>
		public void Close() {
			Statistics.Genaration = -1;
		}

		/// <summary>
		/// This function deterimes if a tile at a position is dead or empty.
		/// </summary>
		/// <param name="candidate"> The position to check in the game's formation matrix</param>
		/// <returns> Whether the tile in the requested position is empty(true) or living(false). </returns>
		public bool IsTileEmpty(TilePosition candidate) {
			Tile t = TileGrid[candidate.GetX(), candidate.GetY()];
			return t == null || t.IsDead();
		}

		/// <summary>
		/// Draws a board in a control's Graphics, empty tile as hollow rectangles and static living ones as full rectangles.
		/// If it incounters an entity tile it delegates it's rendering to the tile.
		/// </summary>
		/// <param name="graphics"> The graphics object to draw to the control with </param>
		/// <param name="size"> The size of the containing control</param>
		/// <param name="xOffset"> The starting x co-ordinate of the control that is usable by the board</param>
		/// <param name="yOffset"> The starting y co-ordinate of the control that is usable by the board</param>
		public void Draw(Graphics graphics, Size size, int xOffset, int yOffset) {
			//int offsetY = 23;
			int scale = GetScale(size, xOffset, yOffset);
			//int width = 3;
			//int height = 3;
			for(int x = 0; x < Width; x++)
				for(int y = 0; y < Height; y++) {
					if(TileGrid[x, y] is EntityTile tile)
						tile.Draw(graphics, scale, xOffset, yOffset);
					else if((!TileGrid[x, y]?.IsDead() ?? false))
						graphics.FillRectangle(Brushes.Black, new Rectangle(xOffset + x * scale, yOffset + y * scale, scale, scale));
					else
						graphics.DrawRectangle(Pens.Black, new Rectangle(xOffset + x * scale, yOffset + y * scale, scale, scale));
				}
		}

		/// <returns> The width of this board </returns>
		public int GetWidth() {
			return Width;
		}
		
		/// <returns> The height of this board </returns>
		public int GetHeight() {
			return Height;
		}
		
		/// <summary>
		/// This function prints how the board will look in the next turn.
		/// </summary>
		public void PrintBoardSchedule() {
			int i = 0;
			//For each tile
			for (int x = 0; x < Width; x++)
				for (int y = 0; y < Height; y++) {
					//Get the current tiles.
					var tile = TileGrid[x, y];
					if (tile != null) {
						Console.Write("Scheduled:");
						//If dead print it's scheduled for death, otherwise print living Tile at.
						if (tile.IsDead())
							Console.WriteLine("Dead Tile at:{0},{1} ,TileType: {2}", x, y, tile);
						else {
							Console.WriteLine("Living Tile at:{0},{1} ,TileType: {2} ", x, y, tile);
							i++;
						}
					}
				}
			//Print how many tiles remain alive.
			Console.WriteLine("{0} Cells will remain Alive", i);
		}
		
		/// <summary>
		/// This function Prints The players details to the console.
		/// </summary>
		public void PrintPlayer() {
			Console.WriteLine("{2} Player is {3} In Pos: x = {0}, y = {1}", Player.GetPosition()
				.GetX(), Player.GetPosition().GetY(), Player.GetName(), Player);
		}
		
		/// <summary>
		/// This function prints the whole board for this turn.
		/// </summary>
		public void PrintBoard() {
			int i = 0;
			//For each tile
			for (int x = 0; x < Width; x++)
				for (int y = 0; y < Height; y++) {
					//Print Dead if null, else print Alive
					Console.WriteLine("Status: {0} For TileType: {1}", TileGrid[x, y] == null ? "Dead" : "Alive", TileGrid[x, y]);
					if (TileGrid[x, y] != null)
						i++;
				}
			//Print the number of tilss that are alive.
			Console.WriteLine("{0} Cells are Still Alive", i);
		}
		
		/// <summary>
		/// This function exports the game's formation matrix to a string.
		/// </summary>
		/// <returns> A string representing the game's formation matrix. </returns>
		public String ExportTiles() {
			//Using StringBuilder because of an addition loop.
			StringBuilder result = new StringBuilder();
			//For each tile
			for (int x = 0; x < Width; x++) {
				for (int y = 0; y < Width; y++)
					//Print 1 for alive tile and 0 for dead tile.
					result.Append(TileGrid[x, y] != null && !TileGrid[x, y].IsDead() ? 1 + "\t" : 0 + "\t");
				//Next Column.
				result.Append('\n');
			}
			return result.ToString();
		}
		
		/// <summary>
		/// This factory method creates a board with an interactable(player) cell.
		/// <summary>
		/// <param name="height"> The height of the generated board object. </param>
		/// <param name="width"> The width of the generated board object. </param>
		/// <param name="et"> The interactable cell. </param>
		public static Board WithPlayer(int height, int width, [NotNull] PlayerTile et) {
			//Ensures the player is not null.
			ValidateArg.HandleParameters(new StackFrame(0), et);
			//Generate board.
			Board b = new Board(height, width);
			//Put the player tile in the right position.
			b.TileGrid[et.GetPosition().GetX(), et.GetPosition().GetY()] = et;
			b.Player = et;
			return b;
		}

		///<returns> The number of generations passed. </returns>
		public int GetGen() {
			return Statistics.Genaration;
		}

		/// <summary>
		/// This function checks a position for neighboring tiles.
		/// </summary>
		/// <param name="tp"> The position to check neighbors for. </param>
		/// <returns> The number of neighbor(living) cells. </returns> 
		public int GetNeighbors(TilePosition tp) {
			var neighbors = 0;
			//Get the co-ordinates of the position.
			var fx = tp.GetX();
			var fy = tp.GetY();
			
			//A circular loop.
			// -1, -1 -> 0, -1 -> 1, -1 ->
			// -1, 0  ->       -> 1, 0  ->
			// -1, 1  -> 0, 1  -> 1, 1.
			for (var i = 0; i < 8; i++) {
				var x = (i > 3) ? (i + 1) % 3 : i % 3;
				x += +fx - 1;
				
				var y = (i > 3) ? (i + 1) / 3 : i / 3;
				y += +fy - 1;
				
				if (x >= 0 && x < TileGrid.GetLength(0) &&
				    y >= 0 && y < TileGrid.GetLength(1) &&
					TileGrid[x, y] != null) {
						//If tile is not null add to neighbors.
						neighbors++;	
				}
			}
			return neighbors;
		}

		/// <summary>
		/// This function calculates wether a cell should be revived or not.
		/// </summary>
		/// <param name="tp"> The position of the tile to check for revival </param>
		/// <returns> true if the cell should be revived, false otherwise. </returns>
		bool IsRevived(TilePosition tp) {
			//By the rules of GOL a tile with 3 neighbors should be revived.
			return GetNeighbors(tp) == 3;
		}
		
		/// <returns> The interactable cell(Player) of the game, if it has any. </returns>
		public PlayerTile GetPlayer() {
			return Player;
		}
		
		/// <summary>
		/// Updates an entity's position to the one given to it.
		/// </summary>
		/// <param name="et"> The entity to update the position of. </param>
		/// <param name="tp"> The new position of the entity. </param>
		public void SetEntityPos(EntityTile et, TilePosition tp) {
			Tile tempTile = TileGrid[tp.GetX(), tp.GetY()];
			TileGrid[et.GetPosition().GetX(), et.GetPosition().GetY()] = tempTile;
			TileGrid[tp.GetX(), tp.GetY()] = et;
		}
		
	}

	/// <summary>
	/// This class is the statistics object of the game.
	/// </summary>
	public class Statistics {
		//The turn number/ generations passed.
		public volatile int Genaration;
		//A random for randomizing the board.
		public Random Rand;
		//A lock for user input
		public readonly System.Threading.Mutex Lock = new System.Threading.Mutex();
		//The amount of tiles that are dead/empty in the board in this turn.
		public int SettlementsDead;
		//The amount of cells that died in this game.
		public int SettelmentsDied;
		
		/// <summary>
		/// This function reports a tile deing.
		/// </summary>
		/// <param name="ts"> The tile deing </param>
		public void NotifyDeadTile(Tile ts) {
			SettlementsDead++;
			SettelmentsDied++;
		}
		
		/// <summary>
		/// This function reports the cenceling of death for a tile.
		/// </summary>
		/// <param name="ts"> The tile that shouldn't die. </param>
		public void NotifyDeathCencal(Tile ts) {
			SettlementsDead--;
			SettelmentsDied--;
		}
		
		/// <summary>
		/// This constructor creates a new statistics object.
		/// </summary>
		public Statistics() {
			Rand = new Random();
		}
	}
}
