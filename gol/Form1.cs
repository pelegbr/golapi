﻿using GolApi;
using GolApi.Event;
using GolApi.impl.Tasks;
using GolApi.impl.Tiles.EntityTiles;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gol {
	/// <summery>
	/// This class is a test for GOLAPI and display all it's basic funcitonality in a window.
	/// </summery>
	public partial class Form1 : Form {
		//The main object of the game.
		public Board Board;
		//The interactive tile in the game.
		PlayerTile PlayerTile;
		//The background image of the window.
		Bitmap image = (Bitmap) Image.FromFile(@"../../Background.jpg");
		//A flag for devmode.
		readonly bool IS_DEV_MODE = true;
		//Time until the next turn occurs
		private double TimeToNextTurn;
		
		/// <summary>
		/// This is the consturctor initializing the game.
		/// </summary>
		public Form1() {
			//DEVMODE
			if (IS_DEV_MODE)
				ConsoleUtils.AllocateConsole();
			//Set this window to draw upon resizing.
			ResizeRedraw = true;
			
			//Initiatalizing the internal components for the window.
			InitializeComponent();
			//Optimize rendering to this window.
			DoubleBuffered = true;
			SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
		}
		
		/// <summary>
		/// This funciton draws the window.
		/// </summary>
		/// <param name="sender">The invoker of the function</param>
		/// <param name="e">The arguments for this paint event</param>
		void PaintForm(object sender, PaintEventArgs e) {
			//The area where the background image should be draw in.
			var destinationRect = new RectangleF(0, 0, Width, Height);
			//The rectangle representing the background image's bounds.
			var sourceRect = new RectangleF(0, 0, image.Width, image.Height);
			//Draw the background.
			e.Graphics.DrawImage(
					image,
					destinationRect,
					sourceRect,
					GraphicsUnit.Pixel);
			//Draw the board(and game objects).
			Board.Draw(e.Graphics, Size, 0, trigger.Height);
		}
		
		/// <summary>
		/// This funciton procceses user input for movement of the player.
		/// It then schedules a movement task in the right direction.
		/// </summary>
		/// <param name="msg"> The type of message</param>
		/// <param name="keyData"> The key pressed</param>
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
			//If pressing up or w ascend the player by decreasing the it's y position.
			if(keyData == Keys.Up || keyData == Keys.W) {
				PlayerTile.ScheduleTask(new TaskMove(new MovementEvent(0, -1, 0)));
				//Lazy draw funciton that doesn't redraw the whole board but just the player and swapped tile.
				PlayerTile.DrawMovement(CreateGraphics(), Size, image, Board.GetScale(Size, 0, trigger.Height), 0, trigger.Height);
				return true;
			}
			//If pressing down or s descend the player by increasing the it's y position.
			if(keyData == Keys.Down || keyData == Keys.S) {
				PlayerTile.ScheduleTask(new TaskMove(new MovementEvent(0, 1, 0)));
				//Lazy draw funciton that doesn't redraw the whole board but just the player and swapped tile.
				PlayerTile.DrawMovement(CreateGraphics(), Size, image, Board.GetScale(Size, 0, trigger.Height), 0, trigger.Height);
				return true;
			}
			//If pressing left or a move the player to the left by decreasing the it's x position.
			if(keyData == Keys.Left || keyData == Keys.A) {
				PlayerTile.ScheduleTask(new TaskMove(new MovementEvent(-1, 0, 0)));
				//Lazy draw funciton that doesn't redraw the whole board but just the player and swapped tile.
				PlayerTile.DrawMovement(CreateGraphics(), Size, image, Board.GetScale(Size, 0, trigger.Height), 0, trigger.Height);
				return true;
			}
			//If pressing right or d move the player right by increasing the it's x position.
			if(keyData == Keys.Right || keyData == Keys.D) {
				PlayerTile.ScheduleTask(new TaskMove(new MovementEvent(1, 0, 0)));
				//Lazy draw funciton that doesn't redraw the whole board but just the player and swapped tile.
				PlayerTile.DrawMovement(CreateGraphics(), Size, image, Board.GetScale(Size, 0, trigger.Height), 0, trigger.Height);
				return true;
			}
			//If none of those keys are pressed delagate to base.
			return base.ProcessCmdKey(ref msg, keyData);
		}
		
		/// <summary>
		/// This funciton handles a click at the "end turn" button
		/// </summary>
		/// <param name="sender">The invoker of the function</param>
		/// <param name="e">The arguments for this click event</param>
		void TrigerClick(object sender, EventArgs e) {
			//Disable user input.
			Board.GotoNext = true;
			//Wait until player controls die.
			Thread.Sleep(200);
			//Reset the TimeToNextTurn variable.
			TimeToNextTurn = TimeSpan.FromSeconds(30).TotalMilliseconds;
			//Call the next turn.
			OnGameTick();
		}
		
		/// <summary>
		/// This function generates the next turn for the game.
		/// </summary>
		void OnGameTick() {
			//Generate next turn.
			Board.Generate();
			//Redraw.
			Invalidate();
		}
		
		/// <summary>
		/// This function initializes the game after the window finished loading all GUI controls.
		/// </summary>
		/// <param name="sender"> The invoker of this funciton</param>
		/// <param name="e"> The arguments of the loading event</param>
		void AfterLoading(object sender, EventArgs e) {
			//PlayerTile = new PlayerTile("Client Player", 500, 500);
			//Board = Board.WithPlayer(1000, 1000, PlayerTile);
			//Generate player controlled cell.
			PlayerTile = new PlayerTile("Client Player", 40, 40);
			//Randomize a game layout.
			Board = Board.WithPlayer(80, 80, PlayerTile);
			//Disable player input.
			Board.GotoNext = true;
			//This portion of the code is responsable for the Countdown and automatic turn switching.
			SynchronizationContext context = SynchronizationContext.Current;
			new Thread(() => {
				//Initiatalize the TimeToNextTurn variable.
				TimeToNextTurn = TimeSpan.FromSeconds(30).TotalMilliseconds;
				//Gets the current time in milliseconds.
				double now = Stopwatch.GetTimestamp() * 1000.0 / Stopwatch.Frequency;
				//While the game hasn't stoped.
				while(Board.GetGen() != -1) {
					//Update the "next turn" button's text.
					context.Post(status => {
						if(!Disposing && !IsDisposed)
							trigger.Text = String.Format("Next turn:{0:0.00}f", TimeToNextTurn / 1000);
					}, null);
					//Calculate how much time is left until the next turn.
					TimeToNextTurn -= (Stopwatch.GetTimestamp() * 1000.0 / Stopwatch.Frequency - now);
					//Update the now Variable.
					now = Stopwatch.GetTimestamp() * 1000.0 / Stopwatch.Frequency;
					//Sleep for 10 milliseconds to not create pressure on the GUI thread.
					Thread.Sleep(10);
					//If it is time for the next turn
					if(TimeToNextTurn <= 0) {
						//Go to the next turn.
						OnGameTick();
						//Reset the timer.
						TimeToNextTurn = TimeSpan.FromSeconds(30).TotalMilliseconds;
					}
				}
			}).Start();
			//Enable player input and generate the first turn.
			OnGameTick();
		}

		/// <summary>
		/// This function closes the game's threads when the window is closed.
		/// </summary>
		/// <param name="sender"> The invoker of this funciton</param>
		/// <param name="e"> The arguments of the closing event</param>
		void CloseForm(object sender, EventArgs e) {
			Board.Close();
		}
	}
}
