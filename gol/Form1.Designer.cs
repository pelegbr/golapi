﻿using System.Drawing;

namespace gol {
	partial class Form1 {
		#region Windows Form Designer generated code
		void InitializeComponent() {
			this.trigger = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// trigger
			// 
			this.trigger.AutoSize = true;
			this.trigger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.trigger.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.trigger.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.trigger.Location = new System.Drawing.Point(12, 0);
			this.trigger.Name = "trigger";
			this.trigger.Size = new System.Drawing.Size(91, 28);
			this.trigger.TabIndex = 0;
			this.trigger.Click += TrigerClick;
			this.trigger.Text = "Next turn: 30";
			this.trigger.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.Controls.Add(this.trigger);
			this.KeyPreview = true;
			this.Name = "Form1";
			this.Text = "Gol";
			this.Paint += PaintForm;
			this.Shown += AfterLoading;
			this.FormClosed += CloseForm;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button trigger;
	}
}

